import alias from 'rollup-plugin-alias';
import babel from 'rollup-plugin-babel';
import commonjs from 'rollup-plugin-commonjs';
import del from 'rollup-plugin-delete';
import { eslint } from 'rollup-plugin-eslint';
import path from 'path';
import pkg from './package.json';
import postcss from 'rollup-plugin-postcss';
import resolve from 'rollup-plugin-node-resolve';
import { terser } from 'rollup-plugin-terser';
import visualizer from 'rollup-plugin-visualizer';
import vue from 'rollup-plugin-vue';

const fileTypes = ['.js', '.vue', '.scss', '.svg'];

export default {
	output: {
		name: 'gabes-foot',
		file: 'dist/gabes-foot.js',
		format: 'umd',
		sourcemap: true
	},
	plugins: [
		del({ targets: ['dist/*', 'stats.html'] }),
		commonjs(),
		alias({
			resolve: fileTypes,
			entries: [
				{
					find: /^~\/(?<path>.*)/u,
					replacement: path.resolve(__dirname, 'src', '$1')
				}
			]
		}),
		resolve({
			browser: true,
			extensions: fileTypes
		}),
		vue(),
		eslint({ include: ['src/**/*.js'] }),
		postcss({
			extensions: ['.styl', '.stylus'],
			extract: true
		}),
		babel(),
		terser(),
		visualizer({
			filename: './stats.html',
			sourcemap: true,
			title: `${pkg.name} Bundle Size Statistics`
		})
	]
};
