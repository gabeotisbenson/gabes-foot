import config from './rollup.config.shared.js';
import html from 'rollup-plugin-html2';
import serve from 'rollup-plugin-serve';

export default {
	...config,
	input: './src/dev.js',
	plugins: [
		...config.plugins,
		html({
			template: 'src/index.html'
		}),
		serve({
			contentBase: 'dist',
			port: 3000
		})
	]
};
