import GabesFoot from '~/GabesFoot';
import Vue from 'vue/dist/vue.js';

const app = new Vue(GabesFoot).$mount('footer');

export default app;
